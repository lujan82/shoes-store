import nextConnect from 'next-connect';
import middleware from '../../middleware/database';

const profile = nextConnect();
profile.use(middleware);

/* Get */
profile.get(async (req, res) => {
    let doc = await req.db.collection('users').findOne()
    res.json(doc);
});


export default profile;